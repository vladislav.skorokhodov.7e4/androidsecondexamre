package com.example.examen2repeat

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.examen2repeat.databinding.HeroDetailBinding

class HeroDetail: Fragment() {
    private lateinit var binding: HeroDetailBinding

    private var hero: HeroDataClass? = null
    private var heroPosition: Int? = null
    private val viewModel: HeroViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments?.isEmpty != true) {
            heroPosition = arguments!!.get("heroPosition") as Int
            hero = viewModel.getList()[heroPosition!!]
        }

        binding.nameTextView.setText(hero!!.nombre)
        binding.filmCover.setImageResource(hero!!.imagen)
        binding.progressBar.progress = (hero!!.inteligence)
        binding.progressBar2.progress = (hero!!.durability)
        binding.progressBar3.progress = (hero!!.strengthhh)
        binding.progressBar4.progress = (hero!!.powerrr)
        binding.progressBar5.progress = (hero!!.speeddd)
        binding.progressBar6.progress = (hero!!.combattt)

    }
}