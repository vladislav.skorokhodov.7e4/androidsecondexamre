package com.example.examen2repeat

import androidx.lifecycle.ViewModel
import kotlin.random.Random

class HeroViewModel: ViewModel() {
    private var heroes = mutableListOf<HeroDataClass>()
    private val images = arrayOf(
        R.drawable.a,
        R.drawable.b,
        R.drawable.c
    )
    init {
        for (i in 1..20) {
            heroes.add(HeroDataClass(
                ("HERO #$i"),
                images[Random.nextInt(3)],
                Random.nextInt(100),
                Random.nextInt(100),
                Random.nextInt(100),
                Random.nextInt(100),
                Random.nextInt(100),
                Random.nextInt(100)
            ))
        }
    }

    fun getList() = heroes

    fun updateHero(heroPosition: Int, hero: HeroDataClass) {
        heroes[heroPosition] = hero
    }
}