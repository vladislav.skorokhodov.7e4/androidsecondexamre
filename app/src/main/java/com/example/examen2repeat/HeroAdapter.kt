package com.example.examen2repeat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.examen2repeat.databinding.HeroListItemBinding

class HeroAdapter(heroes: List<HeroDataClass>): RecyclerView.Adapter<HeroAdapter.HeroViewHolder>() {

    val heroes = heroes

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroViewHolder {

        val v = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.hero_list_item, parent, false)
        return HeroViewHolder(v)
    }

    override fun onBindViewHolder(holder: HeroViewHolder, position: Int) {
        holder.bindData(heroes[position])

        holder.itemView.setOnClickListener {
            val directions = HeroListActivityDirections.actionHeroListActivityToHeroDetail2(position)
            Navigation.findNavController(it).navigate(directions)
        }
    }

    override fun getItemCount(): Int = heroes.size

    class HeroViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private var coverImage: ImageView = itemView.findViewById(R.id.cover_image)
        private var titleText: TextView = itemView.findViewById(R.id.title_text)

        fun bindData(heroDataClass: HeroDataClass){
            coverImage.setImageResource(heroDataClass.imagen)
            titleText.text = heroDataClass.nombre
        }
    }
}